from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'signin.html'}, name='signin'),
    url(r'^$', 'QuantocyteDjangotest.test_app.views.home', name='home'),
    url(r'^signout/$', 'QuantocyteDjangotest.test_app.views.signout', name='signout'),
    url(r'^get_all_items/$', 'QuantocyteDjangotest.test_app.views.get_all_items', name='get_all_items'),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^manager/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    url(r'^manager/', include(admin.site.urls)),
)
