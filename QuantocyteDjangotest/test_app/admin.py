from django.contrib import admin
from QuantocyteDjangotest.test_app.models import Items

__author__ = 'user'

class ItemsAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
    list_editable = ('name',)
    fieldsets = ((None, {'fields': ('name',)}),)

admin.site.register(Items, ItemsAdmin)