from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from QuantocyteDjangotest.test_app.utils import web_response, web_response_redirect
from QuantocyteDjangotest.test_app.models import Items
from django.http import HttpResponse
import json

@login_required
def home(request):
    return web_response(request, 'index.html')

def signout(request):
    logout(request)
    return web_response_redirect(request, 'home')

@login_required
def get_all_items(request):
    return HttpResponse(json.dumps(list(Items.objects.values('id', 'name'))), content_type='json')