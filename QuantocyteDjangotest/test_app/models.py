from django.db import models

__author__ = 'user'

class Items(models.Model):
    id = models.AutoField(primary_key=True, verbose_name='ID', db_column='id')
    name = models.CharField(max_length=100, verbose_name='Name', blank=False, null=False, db_column='name')

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['id']
        db_table = 'Items'
        verbose_name = 'Item'
        verbose_name_plural = 'Items'