from django.conf import settings
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

__author__ = 'user'

def web_response(request, template_name, **params):
    return render_to_response(template_name, params, RequestContext(request))


def web_response_redirect(request, to, *args, **kwargs):
    return redirect(to, *args, **kwargs)
