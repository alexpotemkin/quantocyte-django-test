# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Items'
        db.create_table('Items', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True, db_column='id')),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='name')),
        ))
        db.send_create_signal(u'test_app', ['Items'])


    def backwards(self, orm):
        # Deleting model 'Items'
        db.delete_table('Items')


    models = {
        u'test_app.items': {
            'Meta': {'ordering': "['id']", 'object_name': 'Items', 'db_table': "'Items'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'id'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'name'"})
        }
    }

    complete_apps = ['test_app']